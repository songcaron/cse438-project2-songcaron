package com.example.project2_caronsong.db

import android.provider.BaseColumns


class DBSettings {
    companion object {
        const val DB_NAME = "playlist.db"
        const val DB_VERSION = 1
    }
    class DBPlaylistEntry: BaseColumns {
        companion object {
            const val TABLE = "playlist"
            const val ID = BaseColumns._ID
            const val COL_SONGTITLE = "songTitle"
            const val COL_SONGARTIST = "songArtist"
        }
    }
}

