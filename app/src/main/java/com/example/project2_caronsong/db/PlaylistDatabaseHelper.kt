package com.example.project2_caronsong.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class PlaylistDatabaseHelper(context: Context): SQLiteOpenHelper(context, DBSettings.DB_NAME, null, DBSettings.DB_VERSION) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createFavoritesTableQuery = "CREATE TABLE " + DBSettings.DBPlaylistEntry.TABLE + " ( " +
                DBSettings.DBPlaylistEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DBSettings.DBPlaylistEntry.COL_SONGTITLE + " TEXT NULL, " +
                DBSettings.DBPlaylistEntry.COL_SONGARTIST + " TEXT NULL)"



        db?.execSQL(createFavoritesTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + DBSettings.DBPlaylistEntry.TABLE)
        onCreate(db)
    }

    fun addSong(title: String, artist: String) {
        // Gets the data repository in write mode
        val db = this.writableDatabase

// Create a new map of values, where column names are the keys
        val values = ContentValues().apply {
            put(DBSettings.DBPlaylistEntry.COL_SONGTITLE, title)
            put(DBSettings.DBPlaylistEntry.COL_SONGARTIST, artist)
        }

// Insert the new row, returning the primary key value of the new row
        val newRowId = db?.insert(DBSettings.DBPlaylistEntry.TABLE, null, values)
    }
}