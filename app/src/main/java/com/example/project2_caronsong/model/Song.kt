package com.example.project2_caronsong.model

import java.io.Serializable

class Song(): Serializable {
    private var title: String = ""
    private var artist: String = ""
    private var playcount: Int = 0
    private var listeners: Int = 0
    private var duration: Int = 0
    private var imageSem3UrlStrings: ArrayList<String> = ArrayList()
    private var imageCloudUrlStrings: ArrayList<String> = ArrayList()
    private var imageUrl: String = ""

    constructor(
        title: String,
        artist: String,
        playcount: Int,
        listeners: Int,
        duration: Int,
        imageSem3UrlStrings: ArrayList<String>,
        imageCloudUrlStrings: ArrayList<String>,
        imageUrl: String

    ):
    this(){
        this.title = title
        this.artist = artist
        this.playcount = playcount
        this.listeners = listeners
        this.duration = duration
        this.imageCloudUrlStrings = imageCloudUrlStrings
        this.imageSem3UrlStrings = imageSem3UrlStrings
        this.imageUrl = imageUrl
        convertSem3UrlsToCloud()
    }

    private fun convertSem3UrlsToCloud() {
        val sem3UrlPreface = "http://sem3-idn.s3-website-us-east-1.amazonaws.com/"
        val cloudUrlPreface = "https://res.cloudinary.com/du3z7sadc/image/upload/v1/Apple_Products/"

        for (index in this.imageSem3UrlStrings) {
            var urlPost = index.substring(sem3UrlPreface.length until index.length)
            urlPost = urlPost.replace(',', '_')
            imageCloudUrlStrings.add(cloudUrlPreface + urlPost)
        }
    }

    fun getImages(): ArrayList<String> {
        return this.imageCloudUrlStrings
    }

    fun getSem3Images(): ArrayList<String> {
        return this.imageSem3UrlStrings
    }

    fun getSongName(): String {
        return this.title
    }

    fun getArtistName(): String{
        return this.artist
    }

    fun getPlaycount(): Int{
        return this.playcount
    }

    fun getListeners(): Int{
        return this.listeners
    }

    fun getDuration(): Int{
        return this.duration
    }

}