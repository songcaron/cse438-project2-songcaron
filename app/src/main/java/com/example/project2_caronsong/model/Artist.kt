package com.example.project2_caronsong.model

import java.io.Serializable

class Artist(): Serializable {
    private var name: String = ""
//    private var playcount: Int = 0
//    private var listeners: Int = 0
//    private var duration: Int = 0

    // Images
    private var imageSem3UrlStrings: ArrayList<String> = ArrayList()
    private var imageCloudUrlStrings: ArrayList<String> = ArrayList()

    constructor(
        name: String
    ) : this() {
        this.name = name
//        this.playcount = playcount
//        this.listeners = listeners
//        this.duration = duration
    }



    private fun convertSem3UrlsToCloud() {
        val sem3UrlPreface = "http://sem3-idn.s3-website-us-east-1.amazonaws.com/"
        val cloudUrlPreface = "https://res.cloudinary.com/du3z7sadc/image/upload/v1/Apple_Products/"

        for (index in this.imageSem3UrlStrings) {
            var urlPost = index.substring(sem3UrlPreface.length until index.length)
            urlPost = urlPost.replace(',', '_')
            imageCloudUrlStrings.add(cloudUrlPreface + urlPost)
        }
    }

    fun getImages(): ArrayList<String> {
        return this.imageCloudUrlStrings
    }

    fun getSem3Images(): ArrayList<String> {
        return this.imageSem3UrlStrings
    }

    fun getArtistName(): String {
        return this.name
    }

//    fun getPlaycount(): Int{
//        return this.playcount
//    }
//
//    fun getListeners(): Int{
//        return this.listeners
//    }
//
//    fun getDuration(): Int{
//        return this.duration
//    }
}