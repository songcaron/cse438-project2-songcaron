package com.example.project2_caronsong.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.example.project2_caronsong.R
import com.example.project2_caronsong.ViewModel.SongViewModel
import com.example.project2_caronsong.db.PlaylistDatabaseHelper
import com.example.project2_caronsong.model.Song
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.song_details.*

class SongDetail: AppCompatActivity() {
    private lateinit var song: Song
    private lateinit var viewModel: SongViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.song_details)


        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)

        this.loadUI(song)

        addToPlaylist()
    }


    private fun loadUI(song: Song){
        songtitle.text = song.getSongName()
        songartist.text = song.getArtistName()
        songduration.text = song.getDuration().toString()
        songlisteners.text = song.getListeners().toString()
        songplaycount.text = song.getPlaycount().toString()

        val songImage = song.getImages()
        if (songImage.size > 0) {
            Picasso.with(this).load(song.getImages()[0]).into(songcover)
        }
        else {
            // eventually show image not available pic
        }
    }

     fun addToPlaylist (){
         val buttonClick = findViewById<Button>(R.id.addPlaylist)
         buttonClick?.setOnClickListener {
             val db = PlaylistDatabaseHelper(this)
             db.addSong(songtitle.text.toString(), songartist.text.toString())
         }

    }


}