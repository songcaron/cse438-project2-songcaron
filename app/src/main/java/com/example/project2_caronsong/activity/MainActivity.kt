package com.example.project2_caronsong.activity



import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import com.example.project2_caronsong.R
import com.example.project2_caronsong.fragments.Home
import com.example.project2_caronsong.fragments.PlayList
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity() : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragmentAdapter =
            MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter

        tabs_main.setupWithViewPager(viewpager_main)


    }

    class MyPagerAdapter(fm: FragmentManager):FragmentPagerAdapter(fm){


        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    Home()
                }
                else -> PlayList()
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "Home"
                else -> {
                    return "Playlist"
                }
            }
        }
    }


}
