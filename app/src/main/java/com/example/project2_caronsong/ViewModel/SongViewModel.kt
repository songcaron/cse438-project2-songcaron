package com.example.project2_caronsong.ViewModel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import com.example.project2_caronsong.model.Song
import com.example.project2_caronsong.util.QueryUtils


class SongViewModel(application : Application): AndroidViewModel(application) {
    private var _songList: MutableLiveData<ArrayList<Song>> = MutableLiveData()


    fun getTopSongs(): MutableLiveData<ArrayList<Song>> {
        loadSongs("?filter={\"where\":{\"is_new\":1}}")
        return _songList
    }

    private fun loadSongs(query: String) {
       ProductAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Song>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Song>? {
            return QueryUtils.fetchSongData()
        }

        override fun onPostExecute(result: ArrayList<Song>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                // TODO: Tag Items with favorites

                _songList.value = result
            }
        }
    }
}