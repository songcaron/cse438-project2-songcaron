package com.example.project2_caronsong.ViewModel

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import com.example.project2_caronsong.model.Artist
import com.example.project2_caronsong.util.QueryUtils


class ArtistViewModel(application: Application): AndroidViewModel(application) {
    private var _artistList: MutableLiveData<ArrayList<Artist>> = MutableLiveData()

    fun getArtistByQueryText(query: String): MutableLiveData<ArrayList<Artist>> {
        loadArtist("?filter={\"where\":{\"name\":{\"like\":\".*$query.*\",\"options\":\"i\"}}}")
        return _artistList
    }

    private fun loadArtist(query: String) {
        ProductAsyncTask().execute(query)
    }

    @SuppressLint("StaticFieldLeak")
    inner class ProductAsyncTask: AsyncTask<String, Unit, ArrayList<Artist>>() {
        override fun doInBackground(vararg params: String?): ArrayList<Artist>? {
            return QueryUtils.fetchArtistData(params[0]!!)
        }

        override fun onPostExecute(result: ArrayList<Artist>?) {
            if (result == null) {
                Log.e("RESULTS", "No Results Found")
            }
            else {
                // TODO: Tag Items with favorites

                _artistList.value = result
            }
        }
    }



}