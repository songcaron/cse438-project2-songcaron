package com.example.project2_caronsong.fragments

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.project2_caronsong.R
import com.example.project2_caronsong.ViewModel.SongViewModel
import com.example.project2_caronsong.activity.SongDetail
import com.example.project2_caronsong.model.Song
import kotlinx.android.synthetic.main.playlist_item.view.*
import kotlinx.android.synthetic.main.playlist_list.*

@SuppressLint("ValidFragment")

class PlayList(): Fragment() {
    private var adapter = SongAdapter()
    private lateinit var viewModel: SongViewModel
    private var listInitialized = false


    private var PlayList: ArrayList<Song> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.playlist_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        playlist_list.layoutManager = GridLayoutManager(this.context, 2)
        playlist_list.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)

        val observer = Observer<ArrayList<Song>> {
            playlist_list.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    if(p0 >= PlayList.size || p1 >= PlayList.size) {
                        return false;
                    }
                    return PlayList[p0].getSongName() == PlayList[p1].getSongName()
                }

                override fun getOldListSize(): Int {
                    return PlayList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return PlayList[p0] == PlayList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            PlayList = it ?: ArrayList()
        }


        viewModel.getTopSongs().observe(this, observer)
    }



    inner class SongAdapter: RecyclerView.Adapter<SongAdapter.SongViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SongViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.list_item_grid, p0, false)
            return SongViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: SongViewHolder, p1: Int) {
            val song = PlayList[p1]
            p0.songArtist.text = song.getArtistName()

            p0.songTitle.text = song.getSongName()

            p0.row.setOnClickListener {
                val intent = Intent(this@PlayList.context, SongDetail::class.java)
                intent.putExtra("Song", song)
                startActivity(intent)
            }

        }

        override fun getItemCount(): Int {
            return PlayList.size
        }

        inner class SongViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView
            var songTitle: TextView = itemView.plTitle
            var songArtist: TextView = itemView.plArtist
        }
    }
}