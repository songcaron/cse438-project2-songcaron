package com.example.project2_caronsong.fragments
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.project2_caronsong.R
import com.example.project2_caronsong.ViewModel.SongViewModel
import com.example.project2_caronsong.activity.SongDetail
import com.example.project2_caronsong.model.Song
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_home_list.*
import kotlinx.android.synthetic.main.list_item_grid.view.*


@SuppressLint("ValidFragment")
class TopList(context: Context?): Fragment () {
    private var adapter = SongAdapter()
    private lateinit var viewModel: SongViewModel
    private  var parentContext: Context? = context
    private var listInitialized = false


    private var TopList: ArrayList<Song> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_list, container, false)
    }

    override fun onStart() {
        super.onStart()

        top_list_items.layoutManager = GridLayoutManager(this.context, 2)
        top_list_items.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(SongViewModel::class.java)

        val observer = Observer<ArrayList<Song>> {
            top_list_items.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return TopList[p0].getSongName() == TopList[p1].getSongName()
                }

                override fun getOldListSize(): Int {
                    return TopList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return TopList[p0] == TopList[p1]
                }
            })
            result.dispatchUpdatesTo(adapter)
            TopList = it ?: ArrayList()
        }


        viewModel.getTopSongs().observe(this, observer)

        this.listInitialized = true
    }


    inner class SongAdapter: RecyclerView.Adapter<SongAdapter.SongViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SongViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.list_item_grid, p0, false)
            return SongViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: SongViewHolder, p1: Int) {
            val song = TopList[p1]
            val songImage = song.getImages()
            if (songImage.size == 0) {

            }
            else {
                Picasso.with(this@TopList.context).load(songImage[0]).into(p0.songImg)
            }
            p0.songTitle.text = song.getSongName()

            p0.row.setOnClickListener {
                val intent = Intent(this@TopList.parentContext, SongDetail::class.java)
                intent.putExtra("Song", song)
                startActivity(intent)
            }

        }

        override fun getItemCount(): Int {
            return TopList.size
        }

        inner class SongViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView
            var songTitle: TextView = itemView.title
            var songImg: ImageView = itemView.image
        }
    }
}