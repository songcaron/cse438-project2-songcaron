package com.example.project2_caronsong.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.project2_caronsong.R
import kotlinx.android.synthetic.main.fragment_main.*



/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MainFrag.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MainFrag.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MainFrag : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addButton.setOnClickListener {listener?.loadFrag(0); }
        viewButton.setOnClickListener{listener?.loadFrag(1); }
    }
    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(action: Int) {
        listener?.loadFrag(action)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun loadFrag(action: Int)
    }

}
