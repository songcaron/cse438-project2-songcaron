package com.example.project2_caronsong.fragments

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.project2_caronsong.R
import com.example.project2_caronsong.ViewModel.ArtistViewModel
import com.example.project2_caronsong.activity.SongDetail
import com.example.project2_caronsong.model.Artist
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_home_list.*
import kotlinx.android.synthetic.main.list_item_grid.view.*


@SuppressLint("ValidFragment")

class ArtistList(context: Context?, query: String): Fragment() {
    private var adapter = ResultAdapter()
    private var parentContext: Context? = context
    private lateinit var viewModel: ArtistViewModel
    private var listInitialized = false

    private var queryString: String = query
    private var artistList: ArrayList<Artist> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home_list, container, false)
    }

    override fun onStart() {
        super.onStart()
        val displayText = "Search for: $queryString"
        (activity as AppCompatActivity).supportActionBar?.title = displayText

        top_list_items.layoutManager = GridLayoutManager(parentContext, 2)
        top_list_items.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        viewModel = ViewModelProviders.of(this).get(ArtistViewModel::class.java)

        val observer = Observer<ArrayList<Artist>> {
            top_list_items.adapter = adapter
            val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun areItemsTheSame(p0: Int, p1: Int): Boolean {
                    return artistList[p0].getArtistName() == artistList[p1].getArtistName()
                }

                override fun getOldListSize(): Int {
                    return artistList.size
                }

                override fun getNewListSize(): Int {
                    if (it == null) {
                        return 0
                    }
                    return it.size
                }

                override fun areContentsTheSame(p0: Int, p1: Int): Boolean {
                    return (artistList[p0] == artistList[p1])
                }
            })
            result.dispatchUpdatesTo(adapter)
            artistList = it ?: ArrayList()
        }

        viewModel.getArtistByQueryText(queryString).observe(this, observer)

        this.listInitialized = true
    }

    inner class ResultAdapter: RecyclerView.Adapter<ResultAdapter.ResultViewHolder>() {

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultViewHolder {
            val itemView = LayoutInflater.from(p0.context).inflate(R.layout.fragment_home_list, p0, false)
            return ResultViewHolder(itemView)
        }

        override fun onBindViewHolder(p0: ResultViewHolder, p1: Int) {
            val artist = artistList[p1]
            val artistImages = artist.getImages()
            if (artistImages.size == 0) {

            }
            else {
                Picasso.with(this@ArtistList.context).load(artistImages[0]).into(p0.artistImg)
            }
            p0.artistName.text = artist.getArtistName()
//
//            val playcount = artist.getPlaycount()
//            if (price == -1.0) {
//                p0.productPrice.text = getString(R.string.no_pricing)
//            }
//            else {
//                val priceString = NumberFormat.getCurrencyInstance().format(price)
//                p0.productPrice.text = priceString
//            }

            p0.row.setOnClickListener {
                val intent = Intent(this@ArtistList.parentContext, SongDetail::class.java)
                intent.putExtra("Song", artist)
                startActivity(intent)
            }
        }

        override fun getItemCount(): Int {
            return artistList.size
        }

        inner class ResultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
            val row = itemView

            var artistImg: ImageView = itemView.image
            var artistName: TextView = itemView.title
//            var playcount: TextView = itemView.playcount
//            var listeners: TextView = itemView.listeners
//            var duration: TextView = itemView.duration
            //var productPrice: TextView = itemView.product_price
        }
    }
}