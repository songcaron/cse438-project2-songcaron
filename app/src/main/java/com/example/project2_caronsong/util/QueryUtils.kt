package com.example.project2_caronsong.util

import android.text.TextUtils
import android.util.Log
import com.example.project2_caronsong.model.Artist
import com.example.project2_caronsong.model.Song
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.nio.charset.Charset

class QueryUtils {
    companion object {
        private val LogTag = this::class.java.simpleName
        private const val BaseURL = "http://ws.audioscrobbler.com/2.0/?method=chart.gettopartists&api_key=703a241f812a69964e9a871ca4147631&format=json"
        private const val TopSongBaseURL = "http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=703a241f812a69964e9a871ca4147631&format=json"

        fun fetchArtistData(jsonQueryString: String): ArrayList<Artist>? {
            val url: URL? = createUrl("${this.BaseURL}$jsonQueryString")

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractArtistDataFromJson(jsonResponse)
        }

        fun fetchSongData(): ArrayList<Song>? {
            val url: URL? = createUrl(TopSongBaseURL)

            var jsonResponse: String? = null
            try {
                jsonResponse = makeHttpRequest(url)
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem making the HTTP request.", e)
            }

            return extractSongDataFromJson(jsonResponse)
        }


//        fun fetchQuestionData(jsonQueryString: String): ArrayList<Question>? {
//            val url: URL? = createUrl("${this.BaseURL}api.php$jsonQueryString")
//
//            var jsonResponse: String? = null
//            try {
//                jsonResponse = makeHttpRequest(url)
//            }
//            catch (e: IOException) {
//                Log.e(this.LogTag, "Problem making the HTTP request.", e)
//            }
//
//            return extractQuestionDataFromJson(jsonResponse)
//        }

        private fun createUrl(stringUrl: String): URL? {
            var url: URL? = null
            try {
                url = URL(stringUrl)
            }
            catch (e: MalformedURLException) {
                Log.e(this.LogTag, "Problem building the URL.", e)
            }

            return url
        }

        private fun makeHttpRequest(url: URL?): String {
            var jsonResponse = ""

            if (url == null) {
                return jsonResponse
            }

            var urlConnection: HttpURLConnection? = null
            var inputStream: InputStream? = null
            try {
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.readTimeout = 10000 // 10 seconds
                urlConnection.connectTimeout = 15000 // 15 seconds
                urlConnection.requestMethod = "GET"
                urlConnection.connect()

                if (urlConnection.responseCode == 200) {
                    inputStream = urlConnection.inputStream
                    jsonResponse = readFromStream(inputStream)
                }
                else {
                    Log.e(this.LogTag, "Error response code: ${urlConnection.responseCode}")
                }
            }
            catch (e: IOException) {
                Log.e(this.LogTag, "Problem retrieving the product data results: $url", e)
            }
            finally {
                urlConnection?.disconnect()
                inputStream?.close()
            }

            return jsonResponse
        }

        private fun readFromStream(inputStream: InputStream?): String {
            val output = StringBuilder()
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream, Charset.forName("UTF-8"))
                val reader = BufferedReader(inputStreamReader)
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
            }

            return output.toString()
        }
        private fun extractSongDataFromJson(songJson: String?): ArrayList<Song>? {
            if (TextUtils.isEmpty(songJson)) {
                return null
            }

            val songList = ArrayList<Song>()
            try {
                val baseJsonResponse = JSONObject(songJson).getJSONArray("song_details")
                for (i in 0 until baseJsonResponse.length()) {
                    val songObject = baseJsonResponse.getJSONObject(i)


                    songList.add(
                        Song(
                            returnValueOrDefault<String>(songObject, "title") as String,
                            returnValueOrDefault<String>(songObject, "artist") as String,
                            returnValueOrDefault<Int>(songObject, "playcount") as Int,
                            returnValueOrDefault<Int>(songObject, "listeners") as Int,
                            returnValueOrDefault<Int>(songObject, "duration") as Int,
                            returnValueOrDefault<ArrayList<String>>(songObject, "sem") as ArrayList<String>,
                            returnValueOrDefault<ArrayList<String>>(songObject, "cloud") as ArrayList<String>,
                            returnValueOrDefault<String>(songObject, "imgurl") as String



                        )
                    )
                }
            } catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the category JSON results", e)
            }

            return songList
        }

        private fun extractArtistDataFromJson(artistJson: String?): ArrayList<Artist>? {
            if (TextUtils.isEmpty(artistJson)) {
                return null
            }

            val artistList = ArrayList<Artist>()
            try {
                val baseJsonResponse = JSONObject(artistJson).getJSONArray("results")
                for (i in 0 until baseJsonResponse.length()) {
                    val artistObject = baseJsonResponse.getJSONObject(i)

//                    val incorrectAnswers = returnValueOrDefault<JSONArray>(questionObject, "incorrect_answers") as JSONArray?
//                    val incorrectAnswersList = ArrayList<String>()
//                    if (incorrectAnswers != null) {
//                        for (j in 0 until incorrectAnswers.length()) {
//                            incorrectAnswersList.add(incorrectAnswers.getString(j))
//                        }
//                    }
                    artistList.add(Artist(
                        returnValueOrDefault<String>(artistObject, "name") as String

                    ))
                }
            }
            catch (e: JSONException) {
                Log.e(this.LogTag, "Problem parsing the category JSON results", e)
            }

            return artistList

        }

        private inline fun <reified T> returnValueOrDefault(json: JSONObject, key: String): Any? {
            when (T::class) {
                String::class -> {
                    return if (json.has(key)) {
                        json.getString(key)
                    } else {
                        ""
                    }
                }
                Int::class -> {
                    return if (json.has(key)) {
                        json.getInt(key)
                    }
                    else {
                        return -1
                    }
                }
                Double::class -> {
                    return if (json.has(key)) {
                        json.getDouble(key)
                    }
                    else {
                        return -1.0
                    }
                }
                Long::class -> {
                    return if (json.has(key)) {
                        json.getLong(key)
                    }
                    else {
                        return (-1).toLong()
                    }
                }
                JSONObject::class -> {
                    return if (json.has(key)) {
                        json.getJSONObject(key)
                    }
                    else {
                        return null
                    }
                }
                JSONArray::class -> {
                    return if (json.has(key)) {
                        json.getJSONArray(key)
                    }
                    else {
                        return null
                    }
                }
                else -> {
                    return null
                }
            }
        }
    }
}